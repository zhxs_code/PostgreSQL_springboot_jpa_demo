package com.zxl.postgrespringdemo;

import com.zxl.postgrespringdemo.dao.UserRepository;
import com.zxl.postgrespringdemo.entity.UserDemo;
import com.zxl.postgrespringdemo.pojo.Address;
import com.zxl.postgrespringdemo.pojo.UserBaseInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PostgreSpringDemoApplicationTests {

	@Autowired
	private UserRepository userRepository;


	@Test
	public void saveUserDemo() {
		UserDemo user = new UserDemo();
		UserBaseInfo info = new UserBaseInfo();
		info.setAge(99);
		info.setBirthday(new Date());
		info.setIdCard(99999999L);
		info.setMoney(99.99);
		info.setName("小爷");
		user.setAddress(new Address("测试街", "四川省"));
		user.setInfo(info);
		user = userRepository.save(user);
		System.out.println("保存成功! user : " + user);

	}


	@Test
	public void testFindUserDemo() {
		List<UserDemo> list = userRepository.findByAddress_Province("四川省");
		System.out.println("查询结果" + list);
	}

	@Test
	public void updateUserDemoInfo() {
		List<UserDemo> list2 = userRepository.findByInfo_Name("小爷");
		System.out.println("查询结果" + list2);
		UserDemo user=list2.get(0);
		user.getInfo().setName("王小二");
		userRepository.save(user);
		System.out.println("查询结果" + userRepository.findByInfo_Name("王小二"));

	}
}

