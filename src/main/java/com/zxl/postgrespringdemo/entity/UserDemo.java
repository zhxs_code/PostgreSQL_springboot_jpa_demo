package com.zxl.postgrespringdemo.entity;

import com.zxl.postgrespringdemo.config.JsonbType;
import com.zxl.postgrespringdemo.pojo.Address;
import com.zxl.postgrespringdemo.pojo.UserBaseInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

/**
 * 用户demo类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user_demo")
@TypeDefs({
        @TypeDef(name = "infoType", typeClass = JsonbType.class, parameters = {
                @Parameter(name = JsonbType.CLASS, value = "com.zxl.postgrespringdemo.pojo.UserBaseInfo")
        }),
        @TypeDef(name = "addressType", typeClass = JsonbType.class, parameters = {
                @Parameter(name = JsonbType.CLASS, value = "com.zxl.postgrespringdemo.pojo.Address")
        })
})
public class UserDemo {

    @Id
    @GeneratedValue
    private Long id;

    @Column(columnDefinition = "jsonb")
    @Type(type = "addressType")
    private Address address;


    @Column(columnDefinition = "jsonb")
    @Type(type = "infoType")
    private UserBaseInfo info;
}
