package com.zxl.postgrespringdemo.dao;

import com.zxl.postgrespringdemo.entity.UserDemo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserDemo, Long> {

    // postgre 原生sql语句查询。
    @Query(value = "select * from user_demo as a where  (a.address ->> 'province') = ?1", nativeQuery = true)
    List<UserDemo> findByAddress_Province(String value);

    @Query(value = "select * from user_demo as a where  (a.info ->> 'name') = ?1", nativeQuery = true)
    List<UserDemo> findByInfo_Name(String name);
}
