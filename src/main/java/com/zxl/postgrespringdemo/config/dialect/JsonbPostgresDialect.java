package com.zxl.postgrespringdemo.config.dialect;

import org.hibernate.dialect.PostgreSQL94Dialect;
import org.hibernate.type.StringType;

import java.sql.Types;

public class JsonbPostgresDialect extends PostgreSQL94Dialect {
    public JsonbPostgresDialect() {
        super();
        registerColumnType(Types.JAVA_OBJECT, "jsonb");
        registerHibernateType(Types.ARRAY, StringType.class.getName());
    }


}
