package com.zxl.postgrespringdemo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserBaseInfo {

    private String name;

    private int age;

    private boolean married;

    private double money;

    private Long idCard;

    private Date birthday;
}
